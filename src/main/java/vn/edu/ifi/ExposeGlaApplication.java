package vn.edu.ifi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExposeGlaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExposeGlaApplication.class, args);
	}
}
