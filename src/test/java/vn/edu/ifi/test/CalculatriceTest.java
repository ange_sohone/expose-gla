package vn.edu.ifi.test;

import static org.junit.Assert.*;

import org.junit.Test;

import vn.edu.ifi.models.Calculatrice;

public class CalculatriceTest {
    Calculatrice operation = new Calculatrice();
    int somme = operation.addition(10, 2);
    int resultatTest=12;
    
	@Test
	public void test() {
		System.out.println("Test :"+ somme + " = "+resultatTest);
		assertEquals(somme,resultatTest);
	}

}
